//Tworzymy tablice
const movies = [
  {
    id: 1,
    pic: "images/hobbit.jpg",
    title: "Hobbit",
    desc:
      "A reluctant Hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it from the dragon Smaug.",
    prodDate: "2012",
    mins: "584 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/OPVWy1tFXuc"
  },
  {
    id: 2,
    pic: "images/lotr.jpg",
    title: "The Lord of the Rings",
    desc:
      "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
    prodDate: "2001",
    mins: "684 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/V75dMMIW2B4"
  },
  {
    id: 3,
    pic: "images/warcraft.jpg",
    title: "Warcraft",
    desc:
      "The peaceful realm of Azeroth stands on the brink of war as its civilization faces a fearsome race of invaders: Orc warriors fleeing their dying home to colonize another. As a portal opens to connect the two worlds, one army faces destruction and the other faces extinction.",
    prodDate: "2016",
    mins: "123 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/2Rxoz13Bthc"
  }
];

//Tworzymy
const moviesElements = movies.map(function(movie) {
  return React.createElement(
    "li",
    { key: movie.id },
    React.createElement(
      "div",
      { className: "film-flex" },
      React.createElement("img", { src: movie.pic }),
      React.createElement(
        "div",
        { className: "info-flex" },
        React.createElement("h2", { className: "movie-title" }, movie.title),
        React.createElement(
          "div",
          { className: "date-mins-flex" },
          React.createElement("h4", { className: "prod-date" }, movie.prodDate),
          React.createElement("h4", { className: "film-lenght" }, movie.mins),
          React.createElement("h4", { className: "film-type" }, movie.filmType)
        ),
        React.createElement("p", {}, movie.desc),
        React.createElement(
          "button",
          { className: "trailer-btn" },
          React.createElement(
            "a",
            { href: movie.trailer, target: "_blank" },
            "Trailer"
          )
        )
      )
    )
  );
});

// Umieszczamy powyższe w liscie
const element = React.createElement(
  "div",
  {},
  React.createElement(
    "div",
    { className: "container" },
    React.createElement("h1", { className: "main-header" }, "Movies list"),
    React.createElement("ul", { className: "movie-list" }, moviesElements)
  )
);

ReactDOM.render(element, document.getElementById("app"));
